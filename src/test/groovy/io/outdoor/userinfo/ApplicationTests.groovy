package io.outdoor.userinfo

import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.SpringApplicationConfiguration
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner
import org.springframework.test.context.web.WebAppConfiguration
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import org.springframework.web.context.WebApplicationContext

import static org.hamcrest.Matchers.is
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.asyncDispatch
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner)
@SpringApplicationConfiguration(classes = App)
class ApplicationTests {

	MockMvc mvc

	@Autowired
	WebApplicationContext wac


	@Before
	void setup() {
		mvc = MockMvcBuilders.webAppContextSetup(wac).build()
	}

	@Test
	void contextLoads() {
	}

	@Test
	void getsUserInfo() {
		def action = mvc.perform(get("/users/{user}", "auth0|5666f8f33ddd8f946655ecb1"))
				.andExpect(request().asyncStarted()).andReturn()
		mvc.perform(asyncDispatch(action))
				.andExpect(status().isOk())
				.andExpect(jsonPath("\$.id", is("auth0|5666f8f33ddd8f946655ecb1")))
				.andExpect(jsonPath("\$.email", is("evangelistajoaop@gmail.com")))
				.andExpect(jsonPath("\$.name", is("evangelistajoaop@gmail.com")))

	}

}
