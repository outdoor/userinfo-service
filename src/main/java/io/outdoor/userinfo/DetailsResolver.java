package io.outdoor.userinfo;

import java.util.Collections;
import java.util.Map;

/**
 * @author Joao Pedro Evangelista
 */
public class DetailsResolver {

	private final Md5Encoder encoder;

	public DetailsResolver() {
		encoder = new Md5Encoder();
	}

	public User resolve(Map<String, Object> details) {
		return new User(getUserId(details),
				getEmail(details),
				getPicture(details),
				getName(details),
				getNickname(details),
				isEmailVerified(details));
	}

	private String getEmail(Map<String, Object> details) {
		return String.valueOf(details.getOrDefault("email", ""));
	}

	private boolean isEmailVerified(Map<String, Object> details) {
		return (Boolean) details.getOrDefault("email_verified", false);
	}

	private String getPicture(Map<String, Object> details) {
		return String.valueOf(details.getOrDefault("picture", getGravatarUrl(details)));
	}

	private String getNickname(Map<String, Object> details) {
		return String.valueOf(details.getOrDefault("nickname", ""));
	}

	private String getName(Map<String, Object> details) {
		return String.valueOf(details.getOrDefault("name", userMetadata(details).getOrDefault("name", "")));
	}

	private String getGravatarUrl(Map<String, Object> details) {
		return encoder.encode(getEmail(details));
	}

	@SuppressWarnings("unchecked")
	private Map<String, Object> userMetadata(Map<String, Object> details) {
		return (Map<String, Object>) details.getOrDefault("user_metadata", Collections.emptyMap());
	}

	private String getUserId(Map<String, Object> details) {
		return String.valueOf(details.getOrDefault("user_id", null));
	}
}
