package io.outdoor.userinfo;

/**
 * @author Joao Pedro Evangelista
 */
public class User {

	private String id;

	private String email;

	private String picture;

	private String name;

	private String nickname;

	private boolean emailVerified;

	public User(String id, String email, String picture, String name, String nickname, boolean emailVerified) {
		this.id = id;
		this.email = email;
		this.picture = picture;
		this.name = name;
		this.nickname = nickname;
		this.emailVerified = emailVerified;
	}

	public String getId() {
		return id;
	}

	public String getEmail() {
		return email;
	}

	public String getPicture() {
		return picture;
	}

	public String getName() {
		return name;
	}

	public String getNickname() {
		return nickname;
	}

	public boolean isEmailVerified() {
		return emailVerified;
	}
}