package io.outdoor.userinfo;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * @author Joao Pedro Evangelista
 */
public class Md5Encoder {

	public String encode(String raw) {
		try {
			byte[] md5s = MessageDigest.getInstance("MD5").digest(raw.getBytes());
			return new String(md5s);
		}
		catch (NoSuchAlgorithmException e) {
			throw new RuntimeException("Can not get MD5 algorithm defaulting hash to empty", e);
		}
	}
}
