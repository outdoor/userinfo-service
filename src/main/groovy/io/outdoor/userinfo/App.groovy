package io.outdoor.userinfo

import com.netflix.hystrix.HystrixCommand
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.SpringApplication
import org.springframework.cache.CacheManager
import org.springframework.cache.annotation.Cacheable
import org.springframework.cloud.client.SpringCloudApplication
import org.springframework.cloud.netflix.feign.FeignClient
import org.springframework.context.annotation.Bean
import org.springframework.data.redis.cache.RedisCacheManager
import org.springframework.data.redis.core.RedisTemplate
import org.springframework.http.ResponseEntity
import org.springframework.scheduling.annotation.EnableAsync
import org.springframework.stereotype.Component
import org.springframework.web.bind.annotation.*

import static java.util.Collections.singletonList
import static org.springframework.util.Assert.hasText

@EnableAsync
@SpringCloudApplication
class App {

	static void main(String[] args) {
		SpringApplication.run App, args
	}

	@Bean
	CacheManager cacheManager(RedisTemplate<String, Object> redisTemplate) {
		def manager = new RedisCacheManager(redisTemplate)
		manager.setCacheNames singletonList("userinfo")
		manager.setDefaultExpiration 10000L
		manager
	}
}

@RequestMapping(path = "/api/v2")
@FeignClient(name = "auth0", url = "\${auth0.domain")
interface Auth0ApiClient {

	@RequestMapping(method = RequestMethod.GET,
			path = "/users/{id}")
	HystrixCommand<Map<String, Object>> getUserById(
			@PathVariable("id") String userId,
			@RequestHeader(name = "Authorization") String authorization)

}

@Component
class UserInfoAcquirer {

	private final String token

	private final Auth0ApiClient client

	private final DetailsResolver detailsResolver

	@Autowired
	UserInfoAcquirer(@Value("\${auth0.token}") String token, Auth0ApiClient client) {
		hasText(token, "API token must be provided to execute the call.")
		this.token = token
		this.client = client
		this.detailsResolver = new DetailsResolver()
	}

	@Cacheable("userinfo")
	rx.Observable<User> acquire(String userId) {
		hasText(userId, "User id must be provided to execute the call.")
		client.getUserById(userId, String.format("Bearer %s", token)).toObservable()
				.map { detailsResolver.resolve(it) }
	}
}

@RestController
class UserInfoController {

	@Autowired
	private UserInfoAcquirer acquirer

	@RequestMapping(path = "/users/{user}", method = RequestMethod.GET)
	rx.Observable<ResponseEntity<User>> getUser(@PathVariable("user") String user) {
		acquirer.acquire(user).map { ResponseEntity.ok(it) }
	}
}
